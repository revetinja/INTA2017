<?php include('header.php')?>

    <div class="row">
      <div class="col-md-6">
      <h3><?php echo $lang['BOOK_STATS']; ?></h3>
      
      	<table class="table table-bordered">
          <tbody>
          
          
            <tr>
              <td class="col-md-8"><?php echo $lang['NEW_LIST']; ?></td>
              <td><?php echo $new = $db->get_var("SELECT count(*) FROM bookings WHERE status='1' AND approval_status='0'"); ?></td>
            </tr>
            <tr>
              <td><?php echo $lang['POSITIVE_LIST']; ?></td>
              <td><?php echo $new = $db->get_var("SELECT count(*) FROM bookings WHERE status='2' AND approval_status='1'"); ?></td>
            </tr>
            <tr>
              <td><?php echo $lang['NEGATIVE_LIST']; ?></td>
              <td><?php echo $new = $db->get_var("SELECT count(*) FROM bookings WHERE status='2' AND approval_status='2'"); ?></td>
            </tr>
            
          </tbody>
        </table> 
      </div>
      <div class="col-md-6"><h3><?php echo $lang['MONETARY_STATS']; ?></h3>
      <table class="table table-bordered">
          <tbody>
          
          
            <tr>
              <td class="col-md-8"><?php echo $lang['TOTAL_CAST_ALL']; ?></td>
              <td>
              <?php
			  	/*Total Cast All*/
				$tcall = $db->get_results("SELECT id, status, approval_status, price, days FROM bookings WHERE status !='0' ");
				
				foreach ( $tcall as $sonuc )
				{
				  $total=  $sonuc->price * $sonuc->days;
				  	
					 $total_cast_all = $total_cast_all + $total;
				}
				
				echo number_format($total_cast_all,2); ?> <?php echo $site_info->currency;
				
				
              ?>
              </td>
            </tr>
            <tr>
              <td><?php echo $lang['POZITIVE_CAST_ALL']; ?></td>
              <td>
				<?php
			  	/*Total Cast POZITIVE*/
				$tcpozitive =$db->get_results("SELECT id, status, approval_status, price, days FROM bookings WHERE status='2' AND approval_status='1' ");
				
				foreach ($tcpozitive as $pozitive )
				{
				  $ptotal=  $pozitive->price * $pozitive->days;
				  	
					 $pozitive_cast_all = $pozitive_cast_all + $ptotal;
				}
				
				echo number_format($pozitive_cast_all,2); ?> <?php echo $site_info->currency;
				
				
              ?>
              
              </td>
            </tr>
            <tr>
              <td><?php echo $lang['NEGATIVE_CAST_ALL']; ?></td>
              <td><?php
			  	/*Total Cast NEGATIVE*/
				$tcnegative =$db->get_results("SELECT id, status, approval_status, price, days FROM bookings WHERE status='2' AND approval_status='2' ");
				
				foreach ( $tcnegative as $negative )
				{
				  $ntotal=  $negative->price * $negative->days;
				  	
					 $negative_cast_all = $negative_cast_all + $ntotal;
				}
				
				echo number_format($negative_cast_all,2); ?> <?php echo $site_info->currency;
				
				
              ?></td>
            </tr>
            
          </tbody>
        </table> 
      
      </div>
      
      
      <div class="col-md-12"><h3>Most recent 10 process</h3>
     <div class="table-responsive">
              <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th><?php echo $lang['NAME']; ?></th>
                    <th><?php echo $lang['EMAIL']; ?> / <?php echo $lang['PHONE']; ?></th>
                    <th><?php echo $lang['TYPE_OF_ROOM']; ?></th>
                    <th><?php echo $lang['IN_OUT_DATE']; ?></th>
                    <th><?php echo $lang['DATE']; ?></th>
                    <th>STATUS</th>
                  </tr>
                </thead>
                <tbody>
                
                  <?php
          				$query = $db->get_results("SELECT * FROM bookings ORDER BY id DESC LIMIT 10");
          				
                  foreach ( $query as $row )
                  {
					
					       $room =  $db->get_row("SELECT id,room_name FROM rooms WHERE id='".$row->room."'");	
				        ?>
                
                  <tr id="<?php echo $row->id; ?>">
                    <td><?php echo $row->name; ?></td>
                    <td><?php echo $row->email; ?> <br> <?php echo $row->phone; ?></td>
                    <td><?php echo $room->room_name; ?></td>
                    <td><?php echo $row->in_date; ?> <br> <?php echo $row->out_date; ?></td>
                    <td><?php echo date($site_info->date_format, $row->time); ?><br> (<?php echo nicetime(date("Y-m-d H:i", $row->time)); ?>)</td>
                    <td>
                    
                  	<?php if($row->status =='0'){
						
						echo "Payment Failed";
						
							}else{
						echo "Successful Book";		
							
							} ?> 
                    </td>
                  </tr>
         		<?php } ?> 
                
                </tbody>
              </table>
               </div>       
      
      </div>
    </div>






		


<?php 

// header("Location: new_booking.php");

?>
    
    
<?php include('footer.php')?>