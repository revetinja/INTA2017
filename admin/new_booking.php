<?php 
include('header.php')

?>

     	<ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>
          <li class="active"><?php echo $lang['NEW_LIST']; ?></li>
        </ol> 
       
        <h3><?php echo $lang['NEW_LIST']; ?></h3>
              <div class="table-responsive">
              <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th><?php echo $lang['NAME']; ?></th>
                    <th><?php echo $lang['EMAIL']; ?> / <?php echo $lang['PHONE']; ?></th>
                    <th><?php echo $lang['TYPE_OF_ROOM']; ?></th>
                    <th><?php echo $lang['IN_OUT_DATE']; ?></th>
                    <th><?php echo $lang['DATE']; ?></th>
                    <th><?php echo $lang['ADMIN']; ?></th>
                  </tr>
                </thead>
                <tbody>
                
                  <?php
          				$query = $db->get_results("SELECT * FROM bookings WHERE status='1' AND approval_status='0' ORDER BY id DESC LIMIT 10");
          				
                  foreach ( $query as $row )
                  {
					
					       $room =  $db->get_row("SELECT id,room_name FROM rooms WHERE id='".$row->room."'");	
				        ?>
                
                  <tr id="<?php echo $row->id; ?>">
                    <td><?php echo $row->name; ?></td>
                    <td><?php echo $row->email; ?> <br> <?php echo $row->phone; ?></td>
                    <td><?php echo $room->room_name; ?></td>
                    <td><?php echo $row->in_date; ?> <br> <?php echo $row->out_date; ?></td>
                    <td><?php echo date($site_info->date_format, $row->time); ?><br> (<?php echo nicetime(date("Y-m-d H:i", $row->time)); ?>)</td>
                    <td>
                  <a href="booking_details.php?booking=<?php echo $row->id; ?>" class="btn btn-info"><span class="glyphicon glyphicon-check"></span> <?php echo $lang['MANAGE']; ?></a>
                  <?php
				  /* 
                    <a data-toggle="modal" data-target="#myModal" data-id="<?php echo $row->id; ?>" class="open-AddBookDialog btn btn-info" href="#addBookDialog"><?php echo $lang['MANAGE']; ?></a>
                    
                    */ ?> 
                    </td>
                  </tr>
         		<?php } ?> 
                
                </tbody>
              </table>
               </div>       
              
             

              
                 
                 
            <button type="button" id="loadpage" class="btn btn-default btn-lg btn-block"> <div class="loading" style="display:none"><img src="../assets/images/loading.gif"> </div> <span><?php echo $lang['MORE_RECORDS']; ?></span></button>
            
            
            
            
	<script type="text/javascript">
		$(function(){
			$("#loadpage").click(function(){
				var id  = $("tbody tr:last").attr("id");
				var t = $(this);
				$("span",this).hide();
				$(".loading",this).show();
				$.ajax( {
					type: "POST",
					url :	"../ajax/new_booking.php",
					data : {"id":id},
					success: function (veri) {
						
						$("tbody").append(veri);
						
						$(".loading",t).hide();
						$("span",t).show();
						
					}
				});
				return false;

			
			});		 
		});
		</script>
        
        
 	<script type="text/javascript">
			$(document).on("click", ".open-AddBookDialog", function () {
				 var id = $(this).data('id');
				 $(".modal-body #id").val( id );
			});
	</script> 
    
    
    <!-- Form Post -->
    <script type="text/javascript">
    $(document).ready(function(){
        $(".approval").click(function(){
          
			var data = $("#form-approval").serialize();
			
            $.ajax({
                type	: "POST",
                url 	: "../ajax/approval.php",
                data 	: data,
                success : function(q)
					{
                    $("#approval-div").html(q);
					}
            	});
            return false;
        });
    });
    </script>        
       

    
    
    
<?php include('footer.php')?>