<?php
session_start();
ob_start();
include ('../includes/db_config.php');
include ("../language/".$site_info->language);
if(isSet($_SESSION["password"]))
	{
	header("Location: index.php");
	exit;
	}


	?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.png">

    <title>Admin Panel</title>

    <!-- Bootstrap core CSS -->
	<link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="../assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>

    <!-- Custom styles for this template -->
    <link href="../assets/css/navbar.css" rel="stylesheet">


    <script src="../assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>


  </head>
  <body>

 <div class="container">
 <div class="col-md-6 col-md-offset-3">


  <div class="panel panel-info">
  <div class="panel-heading"><?php echo $lang['ADMIN_LOGIN_FORM']; ?></div>
  <div class="panel-body">
  <div id="settings-div"></div>
        <form class="form-horizontal" action="post" id="login-form" role="form">


          <input type="hidden" name="id" id="id" value="">


          <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['EMAIL']; ?></label>
            <div class="col-sm-6">
              <input type="text" name="email" class="form-control">
            </div>
          </div>

           <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['PASSWORD']; ?></label>
            <div class="col-sm-6">
              <input type="password" name="password" class="form-control">
            </div>
          </div>

            <div class="form-group">
            <label class="col-sm-4 control-label"> </label>
            <div class="col-sm-8">
            <img src="captcha.php" id="captcha" />
              <button type="button" id="change-image" onClick="document.getElementById('captcha').src='captcha.php?'+Math.random();
            document.getElementById('captcha-form').focus();" class="btn btn-gray"><span class="glyphicon glyphicon-refresh"></span> </button>
            </div>
          </div>


          <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['SECURITY_CODE']; ?></label>
            <div class="col-sm-6">
              <input type="text" name="captchaa" class="form-control">




            </div>
          </div>



          <div class="form-group">
            <label class="col-sm-4 control-label"></label>
            <div class="col-sm-8">
              <button type="button" class="login btn btn-primary"><?php echo $lang['LOGIN']; ?></button>
            </div>
          </div>



			</form>
  </div>
</div>


</div>
	<!-- Form Post -->
    <script type="text/javascript">
    $(document).ready(function(){
        $(".login").click(function(){
            var data = $("#login-form").serialize();
            $.ajax({
                type	: "POST",
                url 	: "../ajax/login.php",
                data 	: data,
                success : function(q)
					{
                    $("#settings-div").html(q);
					}
            	});
            return false;
        });
    });
    </script>



</div>
