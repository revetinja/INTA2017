<?php
session_start();
ob_start();
include ('../includes/db_config.php');
include ("../language/".$site_info->language);
include ('../includes/functions.php');


  if(!isSet($_SESSION["password"]))
	{
	header("Location: login.php");
	exit;
	}


  $user_info = $db->get_row("SELECT * FROM users WHERE id ='".$_SESSION["user_id"]."'");

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.png">

    <title><?php echo $site_info->hotel_name; ?> | <?php echo $lang['ADMIN']; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="../assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>

    <!-- Custom styles for this template -->
    <link href="../assets/css/navbar.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- JavaScript plugins (requires jQuery) -->
    <script src="../assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>


  </head>
  <body>

 <div class="container">

      <!-- Static navbar -->
      <div class="navbar navbar-default">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><?php echo $site_info->hotel_name; ?></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Home</a></li>


          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $user_info->email; ?> - [<?php echo $lang['ADMIN']; ?>] <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="settings.php"><?php echo $lang['SETTINGS']; ?></a></li>
                <li><a href="profile.php"><?php echo $lang['MY_PROFILE']; ?></a></li>
                <li class="divider"></li>
                <li><a href="logout.php"><?php echo $lang['LOGOUT']; ?></a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>

      <!-- Stack the columns on mobile by making one full-width and the other half-width -->
		<div class="row">
		  <div class="col-md-3">
            <div class="list-group">
              <a href="#" class="list-group-item active">
                Main Menu
              </a>
              <a href="index.php" class="list-group-item">
			  Home
              </a>
              <a href="new_booking.php" class="list-group-item"><span class="badge pull-right">
			  <?php echo $new = $db->get_var("SELECT count(*) FROM bookings WHERE status='1' AND approval_status='0'"); ?></span> <?php echo $lang['NEW_LIST']; ?>
              </a>

              <a href="all_booking.php" class="list-group-item"><span class="badge pull-right">
			  <?php echo $old = $db->get_var("SELECT count(*) FROM bookings WHERE status='2'"); ?></span><?php echo $lang['ALL_LIST']; ?>
              </a>

              <a href="rooms.php" class="list-group-item"><span class="badge pull-right"></span><?php echo $lang['MANAGE_ROOMS']; ?></a>
              <a href="settings.php" class="list-group-item"><span class="badge pull-right"></span><?php echo $lang['SETTINGS']; ?></a>
            </div>



		  </div>
		  <div class="col-md-9">
