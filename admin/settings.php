<?php include('header.php')?>

   		<ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>
          <li class="active"><?php echo $lang['SETTINGS']; ?></li>
        </ol>


<div class="panel panel-info">
  <div class="panel-heading"><?php echo $lang['HOTEL_SETTINGS']; ?></div>
  <div class="panel-body">
  <div id="settings-div"></div>
        <form class="form-horizontal" action="post" id="settings-form" role="form">


          <input type="hidden" name="id" id="id" value="">


          <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['HOTEL_NAME']; ?></label>
            <div class="col-sm-8">
              <input type="text" name="hotel_name" class="form-control" value="<?php echo $site_info->hotel_name;?>">
            </div>
          </div>



<!--          <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['DEFAULT_LANGUAGE']; ?></label>
            <div class="col-sm-4">
              <select name="language" class="form-control">
              <option value="<?php echo $site_info->language;?>"><?php echo $site_info->language;?> (<?php echo $lang['DEFAULT']; ?>)</option>
				<?php
                $dir = opendir("../language/");

                while (($file = readdir($dir)) !== false)
                {

                if(! is_dir($file)){

				if($file == $site_info->language){


					}else{
				?>
				<option value="<?php echo $file; ?>"><?php echo $file; ?></option>
				<?php
					}
                }}
                closedir($dir);
                ?>
                </select>
            </div>
          </div> -->





          <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['DEFAULT_EMAIL']; ?></label>
            <div class="col-sm-8">
              <input type="text" name="email" class="form-control" value="<?php echo $site_info->email;?>">
            </div>
          </div>

   <div class="form-group">
            <label class="col-sm-4 control-label"><?php

            echo $lang['DATE_FORMAT']; ?></label>
            <div class="col-sm-2">
              <input type="text" name="date_format" class="form-control" value="<?php echo $site_info->date_format;

              ?>">
            </div>
          </div> 






<!--         <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['PAYPAL_PAY']; ?></label>
            <div class="col-sm-4">
              <select name="paypal_pay" class="form-control">

              		<option value="<?php echo $site_info->paypal_pay; ?>"><?php if($site_info->paypal_pay =='1') { echo $lang['ENABLE']; }else{ echo $lang['DISABLE']; } ?></option>
              	<?php

					if($site_info->paypal_pay=='0'){
					?>
					<option value="1"><?php echo $lang['ENABLE']; ?></option>
					<?php
                    }else{
					?>
					<option value="0"><?php echo $lang['DISABLE']; ?></option>
					<?php
					}
					?>

                </select>
            </div>
          </div> -->

<!--		<div class="form-group">
            <label class="col-sm-4 control-label"><?php
            /* echo
            $lang['CC_PAY']; ?></label>
            <div class="col-sm-4">
              <select name="cc_pay" class="form-control">

              		<option value="<?php echo $site_info->cc_pay; ?>"><?php if($site_info->cc_pay =='1') { echo $lang['ENABLE']; }else{ echo $lang['DISABLE']; } ?></option>
              	<?php

					if($site_info->cc_pay=='0'){
					?>
					<option value="1"><?php echo $lang['ENABLE']; ?></option>
					<?php
                    }else{
					?>
					<option value="0"><?php echo $lang['DISABLE']; ?></option>
					<?php
        }
        */
					?>

                </select>
            </div>
          </div> -->



		<div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['PAYPAL_EMAIL']; ?></label>
            <div class="col-sm-8">
              <input type="text" name="paypal_email" class="form-control" value="<?php echo $site_info->paypal_email;?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['SYSTEM_BASE']; ?></label>
            <div class="col-sm-8">
              <input type="text" name="sistem_base" class="form-control" value="<?php echo $site_info->sistem_base;?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['CURRENCY']; ?></label>
            <div class="col-sm-2">
              <input type="text" name="currency" class="form-control" value="<?php echo $site_info->currency;?>">
            </div>
          </div>










          <div class="form-group">
            <label class="col-sm-4 control-label"></label>
            <div class="col-sm-8">
              <button type="button" class="settings btn btn-primary"><?php echo $lang['SAVE_CHANGES']; ?></button>
            </div>
          </div>


			</form>
  </div>
</div>

	<!-- Form Post -->
    <script type="text/javascript">
    $(document).ready(function(){
        $(".settings").click(function(){
            var data = $("#settings-form").serialize();
            $.ajax({
                type	: "POST",
                url 	: "../ajax/settings.php",
                data 	: data,
                success : function(q)
					{
                    $("#settings-div").html(q);
					}
            	});
            return false;
        });
    });
    </script>







<?php include('footer.php')?>
