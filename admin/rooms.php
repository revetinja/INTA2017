<?php include('header.php')?>
		<ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>
          <li class="active"><?php echo $lang['ROOM_LIST']; ?></li>
        </ol> 

   <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
		<form class="form-horizontal" id="NewRoomForm" role="form">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $lang['NEW_ROOM_TYPE']; ?></h4>
          </div>
          
          
          <div class="modal-body">
        	<div id="NewRoomDiv"></div>
          
          
          
          <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['ROOM_TYPE']; ?></label>
            <div class="col-sm-8">
            <input type="text" name="room_name" class="form-control">
            </div>
          </div>
          
           <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['SORT']; ?></label>
            <div class="col-sm-2">
            <input type="text" name="sort" class="form-control">
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['PRICE']; ?></label>
            <div class="col-sm-4">
            <input type="text" name="price" class="form-control">
            </div>
          </div>
          
          </div>
          <div class="modal-footer">
            <button type="button" class="NewRoomType btn btn-primary"><?php echo $lang['SAVE_CHANGES']; ?></button>
          </div> 
			</form>
          
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->    


       
       
        <h3><?php echo $lang['ROOM_LIST']; ?></h3>
              <div class="table-responsive">
              
              <div id="room-delete"></div>
              
              
              <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="col-md-1">ID</th>
                    <th ><?php echo $lang['ROOM_TYPE']; ?></th>
                    <th class="col-md-2"><?php echo $lang['PRICE']; ?></th>
                    <th class="col-md-1"><?php echo $lang['SORT']; ?></th>
                    <th class="col-md-2"><?php echo $lang['ADMIN']; ?></th>
              
                  </tr>
                </thead>
                <tbody>
                
                  <?php
        $query = $db->get_results("SELECT * FROM rooms ORDER BY sort DESC");
        foreach ( $query as $row )
        {  
        ?>
                
                   <tr id="<?php echo $row->id; ?>">
                    <td><?php echo $row->id; ?></td>
                    <td><?php echo $row->room_name; ?></td>
                    <td><?php echo number_format($row->price,2); ?> <?php echo $site_info->currency ?></td>
                    <td><?php echo $row->sort; ?></td>
                    <td>
                    <button type="button" id="<?php echo $row->id; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> <?php echo $lang['DELETE']; ?></button>
                    </td>
                    
                  </tr>
         		<?php } ?> 
                
                
                </tbody>
              </table>
              
  
              
              
              
              <button class="btn btn-success" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus"></span> <?php echo $lang['ADD_NEW']; ?></button>
               </div>       
            
  
  
			<script type="text/javascript"> 
            $(document).ready(function() {  
                $('.btn-danger').click(function (){  
            
                     var room_id = $(this).attr("id");
                                $.ajax( {
                                    type: "POST",
                                    url :	"../ajax/delete_room.php",
                                    data : { "room_id":room_id},
                                    success: function (veri) {
                                        
                                        $("#room-delete").html(veri);
                                    }
                                    
                                });
                     
                    $(this).parent('td').parent('tr').animate({ backgroundColor: "#fbc7c7" }, "fast")
                    .animate({ opacity: "hide" }, "slow");  
                 
                });  
            });  
            </script>            
	
    
            <!-- Form Post -->
            <script type="text/javascript">
            $(document).ready(function(){
                $(".NewRoomType").click(function(){
                    var data = $("#NewRoomForm").serialize();
                    $.ajax({
                        type	: "POST",
                        url 	: "../ajax/add_room.php",
                        data 	: data,
                        success : function(q)
                            {
                            $("#NewRoomDiv").html(q);
                            }
                        });
                    return false;
                });
            });
            </script>
    
    
    
<?php include('footer.php')?>