<?php 
include('header.php');


$book = $db->get_row("SELECT * FROM bookings WHERE id='".$_GET['booking']."'");

$room_info = $db->get_row("SELECT id,room_name FROM rooms WHERE id = '".$book->room."'");	
?>

   <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
		<form class="form-horizontal" id="form-approval" role="form">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $lang['BOOKING_CONFIRMATION']; ?></h4>
          </div>
          
          
          <div class="modal-body">
        <div id="approval-div"></div>
          <input type="hidden" name="id" value="<?php echo $book->id; ?>">
          
          
          <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['RESULT']; ?></label>
            <div class="col-sm-8">
              <select name="status" class="form-control">
                  <option value="1"><?php echo $lang['POSITIV_RESAULT']; ?></option>
                  <option value="2"><?php echo $lang['NEGATIVE_RESULT']; ?></option>
                </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['EXPLANATION']; ?></label>
            <div class="col-sm-8">
              <textarea name="note" class="form-control" rows="2"></textarea>
            </div>
          </div>
          
          </div>
          <div class="modal-footer">
            <button type="button" class="approval btn btn-primary"><?php echo $lang['SAVE_CHANGES']; ?></button>
          </div> 
			</form>
          
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->    
    
    
    
    

	




<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $lang['SEND_EMAIL']; ?> <?php echo $book->name;?></h4>
      </div>
      <div class="modal-body">
      
      <div id="form-email-back"></div>
        <form role="form" name="emailform" id="form-email" method="post">
        
          <div class="form-group">
          	<label for="exampleInputEmail1">From:</label>
            <input type="text" class="form-control" name="from_email"  value="<?php echo $site_info->email;?>">
          </div>
          
           <div class="form-group">
           <label for="exampleInputEmail1">To:</label>
            <input type="text" class="form-control" name="user_email" value="<?php echo $book->email;?>">
          </div>
          
          <div class="form-group col">
          	<label for="exampleInputEmail1">Subject:</label>
            <input type="text" name="subject" class="form-control" id="exampleInputEmail1" placeholder="Enter Subject">
          </div>
          
          <div class="form-group">
            <textarea class="form-control" name="message" rows="6"></textarea>
            
          </div>
          
      
      </div>
      <div class="modal-footer">
      	<input type="hidden" name="book" value="<?php echo $book->id; ?>">
        <button type="button" class="emailform btn btn-warning"><span class="glyphicon glyphicon-envelope"></span>  <?php echo $lang['SEND_EMAIL']; ?></button>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


    
    
    
      <ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>
          <li><a href="new_booking.php"><?php echo $lang['NEW_LIST']; ?></a></li>
          <li class="active"><?php echo $book->id; ?> <?php echo $lang['BOOKING']; ?></li>
        </ol>   
       
       <h4>#<?php echo $book->id; ?> <?php echo $lang['BOOKING']; ?></h4> 
       
  <table class="table table-bordered">
       
          <tbody>
            <tr>
              <td class="col-md-4"><?php echo $lang['NAME']; ?></td>
              <td><?php echo $book->name; ?> (<?php echo $book->user_ip; ?>)</td>
            </tr>
            <tr>
              <td><?php echo $lang['EMAIL']; ?></td>
              <td><?php echo $book->email; ?></td>
            </tr>
            <tr>
              <td><?php echo $lang['PHONE']; ?></td>
              <td><?php echo $book->phone; ?></td>
            </tr>
            <tr>
              <td><?php echo $lang['ADULTS']; ?></td>
              <td><?php echo $book->adults; ?></td>
            </tr>
            <tr>
              <td><?php echo $lang['CHILDREN']; ?></td>
              <td><?php echo $book->children; ?></td>
            </tr>
            <tr>
              <td><?php echo $lang['TYPE_OF_ROOM']; ?></td>
              <td><?php echo $room_info->room_name; ?></td>
            </tr>
            <tr>
              <td><?php echo $lang['IN_OUT_DATE']; ?></td>
              <td><?php echo $book->in_date; ?> - <?php echo $book->out_date; ?> </td>
            </tr>
            <tr>
              <td><?php echo $lang['TOTAL_DAYS']; ?></td>
              <td><?php echo $book->days; ?></td>
            </tr>
            <tr>
              <td><?php echo $lang['DATE']; ?></td>
              <td><?php echo date($site_info->date_format, $book->time); ?> (<?php echo nicetime(date("Y-m-d H:i", $book->time)); ?>)</td>
            </tr>
            <tr>
              <td><?php echo $lang['NOTE']; ?></td>
              <td><?php echo $book->comments; ?></td>
            </tr>
            <tr>
              <td><?php echo $lang['TOTAL_PRICE']; ?></td>
              <td><?php echo number_format($book->price,2); ?> x <?php echo $book->days; ?> = <?php $total = $book->price * $book->days; echo number_format($total,2); ?> <?php echo $site_info->currency; ?> </td>
            </tr>
            <tr>
              <td><?php echo $lang['PAYMENT_TYPE']; ?></td>
              <td>
			  <?php if($book->pay_type =='paypal'){
				  
					  	echo $lang['PAYPAL_PAY']; 
						
						}elseif($book->pay_type =='cc'){
						
						echo $lang['CC_PAY']; 
						  
						}elseif($book->pay_type =='hotel'){
						echo $lang['HOTEL_PAY'];	
						}else{}
				  
				  ?>
             
			  </td>
            </tr>
          </tbody>
        </table> 
        
         <hr />
       
        <?php if($book->status =='2'){ ?>
        
        <h4><?php echo $lang['ADMIN_COMMENTS']; ?></h4>
		
        
        
          
        <table class="table table-bordered">
          <tbody>
            <tr>
              <td class="col-md-4"><?php echo $lang['ADMIN_COMMENTS']; ?></td>
              <td><?php echo $book->note; ?></td>
            </tr>
            </tr>
          </tbody>
        </table> 
         <?php } ?> 
          
   
		<hr />
        
        <!--Send Email list   -->
        <h4><?php echo $lang['SEND_EMAIL_LIST']; ?></h4>
        <?php
         $query = $db->get_results("SELECT * FROM email WHERE book='".$book->id."'");
			  foreach ( $query as $row )
			  {
          ?>
          <h4>#<?php echo $row->id; ?></h4>
        <table class="table table-bordered">
          <tbody>
            <tr>
              <td class="col-md-4"><?php echo $lang['SUBJECT']; ?></td>
              <td><?php echo $row->subject; ?></td>
            </tr>
            <tr>
              <td class="col-md-4"><?php echo $lang['MESSAGE']; ?></td>
              <td><?php echo $row->message; ?></td>
            </tr>
            </tr>
          </tbody>
        </table>  
      <?php } ?>
         
      <?php if($book->status !='2'){?>   
     <a data-toggle="modal" data-target="#myModal" data-id="<?php echo $book->id; ?>" class="open-AddBookDialog btn btn-info" href="#addBookDialog">
     <span class="glyphicon glyphicon-floppy-disk"></span> <?php echo $lang['MANAGE']; ?>
     </a> 
     <?php }?>
	
    <a data-toggle="modal" data-target="#emailModal" data-id="<?php echo $book->id; ?>" class="open-AddBookDialog btn btn-warning" href="#addBookDialog">
     <span class="glyphicon glyphicon-envelope"></span> <?php echo $lang['SEND_EMAIL']; ?>
     </a> 

               
       
      

   
    
    
    <!-- Form Manege Post -->
    <script type="text/javascript">
    $(document).ready(function(){
        $(".approval").click(function(){
          
			var data = $("#form-approval").serialize();
			
            $.ajax({
                type	: "POST",
                url 	: "../ajax/approval.php",
                data 	: data,
                success : function(q)
					{
                    $("#approval-div").html(q);
					}
            	});
            return false;
        });
    });
    </script>        
       

    
    <!-- Email Form Post -->
    <script type="text/javascript">
    $(document).ready(function(){
        $(".emailform").click(function(){
          
			var data = $("#form-email").serialize();
			
            $.ajax({
                type	: "POST",
                url 	: "../ajax/send_email.php",
                data 	: data,
                success : function(q)
					{
                    $("#form-email-back").html(q);
					}
            	});
            return false;
        });
    });
    </script>      
    
<?php include('footer.php')?>