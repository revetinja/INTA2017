<?php include('header.php')?>


<?php 
	$data =  mysql_fetch_assoc(mysql_query("SELECT * FROM users WHERE id='".$_SESSION["user_id"]."'"));

?>
 
 <ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>
          <li class="active"><?php echo $lang['MY_PROFILE']; ?></li>
        </ol> 
       
<div class="panel panel-info">
  <div class="panel-heading"><?php echo $lang['MY_PROFILE']; ?></div>
  <div class="panel-body">
  <div id="profile-div"></div>
        <form class="form-horizontal" action="post" id="profile-form" role="form">
         
        
          <input type="hidden" name="id" id="id" value="">
          
          
          <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['NAME']; ?></label>
            <div class="col-sm-8">
              <input name="name" type="text" class="form-control" id="name" value="<?php echo $data['name'];?>">
            </div>
          </div>
          
           <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['EMAIL']; ?></label>
            <div class="col-sm-8">
              <input name="email" type="text" class="form-control" id="email" value="<?php echo $data['email'];?>">
            </div>
          </div>
           
          
          <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['OLD_PASSWORD']; ?></label>
            <div class="col-sm-4">
              <input type="password" name="old_password" class="form-control">
            </div>
          </div> 
          
          <div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['PASSWORD']; ?></label>
            <div class="col-sm-4">
              <input type="password" name="password" class="form-control">
            </div>
          </div>          
       
			<div class="form-group">
            <label class="col-sm-4 control-label"><?php echo $lang['PASSWORD_AGAIN']; ?></label>
            <div class="col-sm-4">
              <input type="password" name="re_password" class="form-control">
            </div>
          </div>  
          
          
          <div class="form-group">
            <label class="col-sm-4 control-label"></label>
            <div class="col-sm-8">
              <button type="button" class="profile btn btn-primary"><?php echo $lang['SAVE_CHANGES']; ?></button>
            </div>
          </div>
          
          
          
			</form>
  </div>
</div>
       
	<!-- Form Post -->
    <script type="text/javascript">
    $(document).ready(function(){
        $(".profile").click(function(){
            var data = $("#profile-form").serialize();
            $.ajax({
                type	: "POST",
                url 	: "../ajax/profile.php",
                data 	: data,
                success : function(q)
					{
                    $("#profile-div").html(q);
					}
            	});
            return false;
        });
    });
    </script>   
        



    
    
    
<?php include('footer.php')?>